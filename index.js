console.log(`Hello World!`);

function getConsonants(str){
    console.log(str);
    let vowels = ["a","e","i","o","u"];
    let consonants = '';

    for (let i = 0; i < str.length; i++) {
        if (vowels.indexOf(str[i].toLowerCase()) >= 0){
            continue;
        } else {
            consonants += str[i];
        }
    }

    console.log(consonants);
}

function checkNumbers(num){
    console.log(`The number you provided is ${num}`);

    for (let i = num; i >= 0; i--) {
        if (i % 5 == 0) {
            console.log(i);
        }

        if (i < 50){
            console.log(`The current value is at ${i}, terminating the loop...`);
            break;
        }
        
        if (i % 10 == 0) {
            console.log(`${i} is divisible by 10, skipping to the next iteration...`);
            continue;
        }
    }
}

let num = prompt('Enter number: ');
let str = `supercalifragilisticexpialidocious`;

checkNumbers(num);
getConsonants(str);